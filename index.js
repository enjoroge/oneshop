const express = require('express')
const app = express()
const dotenv = require('dotenv').config()
const bodyParser = require('body-parser')
const dbConnect = require('./config/db.connect')
const authRouter = require('./routes/authRoute')
const prodRouter = require('./routes/prodRoute')
const { notFound, erroeHandler } = require('./middlewares/errorHandler')
const cookieParser = require('cookie-parser')
const morgan = require("morgan")


const PORT = process.env.PORT || 4000

dbConnect();
app.use(morgan("dev"))
app.use(bodyParser.json())
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:false}))


//routes
app.use('/api/users', authRouter)
app.use('/api/product', prodRouter)
app.use(notFound)
app.use(erroeHandler)
app.use(cookieParser())
app.get('/', (req,res)=>{
    res.send("hello there")
})


app.listen(PORT,()=>{
    console.log(`Server Listening to port ${PORT}`);
})