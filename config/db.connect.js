const mongoose = require("mongoose");
const DBURL = process.env.DBURL;
const dbConnect = async() => {
  try {
    const connect = await mongoose
      .connect(DBURL)
      .then(() => {
        console.log("Database connected successfully");
      });
  } catch {
    (error) => {
      console.log("Database Error");
    };
  }
};
module.exports = dbConnect