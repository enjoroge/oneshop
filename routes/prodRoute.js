const express = require("express");
const router = express.Router();
const {
  createProduct,
  getAllProduct,
  getAProduct,
  updateProduct,
  deleteProduct,
} = require("../controllers/product.controller");
const { isAdmin, authMiddleware } = require("../middlewares/authMiddleware");

router.post("/", authMiddleware,isAdmin, createProduct);
router.get("/", getAllProduct);
router.get("/:id", getAProduct);
router.put("/:id",  authMiddleware,isAdmin, updateProduct);
router.delete("/:id",  authMiddleware,isAdmin, deleteProduct);

module.exports = router;
