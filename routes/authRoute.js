const express = require("express");
const router = express.Router();
const {
  createUser,
  loginUser,
  fetchUsers,
  fetchUser,
  updateUser,
  deleteUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logOut,
  updatePassword
} = require("../controllers/user.controller");
const {authMiddleware,isAdmin} = require('../middlewares/authMiddleware')

router.post("/register", createUser);
router.put("/password",authMiddleware, updatePassword);
router.post("/login", loginUser);
router.get("/logout", logOut);
router.get("/", fetchUsers);

router.get("/refresh",handleRefreshToken);
router.get("/:id", authMiddleware, isAdmin, fetchUser);
router.put("/edit-user",authMiddleware, updateUser);
router.put("/block-user/:id",authMiddleware, blockUser);
router.put("/unblock-user/:id",authMiddleware, unblockUser);
router.delete("/:id", deleteUser);
module.exports = router;
